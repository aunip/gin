package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/aunip/gin/services"
	"gitlab.com/aunip/gin/models"
)

// CreatePizza : Create Pizza (EndPoint)
func CreatePizza(c *gin.Context) {
	var pizza models.IPizza
	_ = json.NewDecoder(c.Request.Body).Decode(&pizza)
	payload := services.AddPizza(pizza)
	if payload != nil {
		c.JSON(http.StatusCreated, payload)
	} else {
		c.JSON(http.StatusInternalServerError, nil)
	}
}

// CreateManyPizzas : Create Many Pizzas (EndPoint)
func CreateManyPizzas(c *gin.Context) {
	var pizzas []models.IPizza
	_ = json.NewDecoder(c.Request.Body).Decode(&pizzas)
	payload := services.AddManyPizzas(pizzas)
	if payload != nil {
		c.JSON(http.StatusOK, payload)
	} else {
		c.JSON(http.StatusInternalServerError, nil)
	}
}

// GetAllPizzas : Get All Pizzas (EndPoint)
func GetAllPizzas(c *gin.Context) {
	payload := services.ReadAllPizzas()
	if payload != nil {
		c.JSON(http.StatusOK, payload)
	} else {
		c.JSON(http.StatusOK, make([]models.IPizza, 0))
	}
}

// GetPizza : Get Pizza (EndPoint)
func GetPizza(c *gin.Context) {
	id := c.Param("id")
	payload := services.ReadPizza(id)
	if payload != nil {
		c.JSON(http.StatusOK, payload)
	} else {
		c.JSON(http.StatusInternalServerError, nil)
	}
}

// UpdatePizza : Update Pizza (EndPoint)
func UpdatePizza(c *gin.Context) {
	id := c.Param("id")
	var pizza interface{}
	_ = json.NewDecoder(c.Request.Body).Decode(&pizza)
	payload := services.ModifyPizza(id, pizza)
	if payload != nil {
		c.JSON(http.StatusOK, payload)
	} else {
		c.JSON(http.StatusNotFound, nil)
	}
}

// ClearAllPizzas : Clear All Pizzas (EndPoint)
func ClearAllPizzas(c *gin.Context) {
	payload := services.DeleteAllPizzas()
	if payload != nil {
		c.JSON(http.StatusOK, payload)
	} else {
		c.JSON(http.StatusInternalServerError, nil)
	}
}

// ClearPizza : Clear Pizza (EndPoint)
func ClearPizza(c *gin.Context) {
	id := c.Param("id")
	payload := services.DeletePizza(id)
	if payload != nil {
		c.JSON(http.StatusOK, payload)
	} else {
		c.JSON(http.StatusNotFound, nil)
	}
}
