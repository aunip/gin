package main

import (
	"fmt"

	"github.com/gin-gonic/gin"

	"gitlab.com/aunip/gin/controllers"
)

// PORT ...
const PORT = "5050"

func main() {
	router := gin.Default()

	router.POST("/api/pizza", controllers.CreatePizza)
	router.POST("/api/pizzas", controllers.CreateManyPizzas)
	router.GET("/api/pizzas", controllers.GetAllPizzas)
	router.DELETE("/api/pizzas", controllers.ClearAllPizzas)
	router.GET("/api/pizza/:id", controllers.GetPizza)
	router.PUT("/api/pizza/:id", controllers.UpdatePizza)
	router.DELETE("/api/pizza/:id", controllers.ClearPizza)
	router.Static("/api-docs", "./public")

	fmt.Println("> Listening On 'http://localhost:" + PORT + "'")
	router.Run(":" + PORT)
}
