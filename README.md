# All U Need Is Pizza

> _Made With **Go** 1.13_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- controllers
    +-- pizza_ctrl.go
+-- models
    +-- payload_model.go
    +-- pizza_model.go
+-- public
    +-- favicon.png
    +-- index.html
    +-- oauth2*.html
    +-- swagger-ui*.{js,css}
    +-- swagger.json
+-- services
    +-- pizza_service.go
+-- .gitignore
+-- go.mod
+-- go.sum
+-- LICENSE
+-- main.go
+-- pizzas.json
+-- README.md
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/gin.git
```

Install:

```
go get -v
```

Launch:

```
go run main.go
```

Build:

```
go build main.go
```

### Requirement

- [x] **MongoDB** Server

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
