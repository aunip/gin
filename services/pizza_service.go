package services

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"

	"gitlab.com/aunip/gin/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// URI : MongoDB URI
const URI = "mongodb://localhost:27017"

// DB : DataBase Name
const DB = "aunip"

// COLL : Collection Name
const COLL = "pizzas"

func initPizzas() {
	var pizzas []models.IPizza

	byteValues, err := ioutil.ReadFile("pizzas.json")
	if err != nil {
		log.Fatal(err)
	}
	json.Unmarshal(byteValues, &pizzas)

	count := len(ReadAllPizzas())

	if count == 0 {
		AddManyPizzas(pizzas)
	} else {
		for _, pizza := range pizzas {
			result := ReadPizzaByLabel(pizza.Label)

			if result == nil {
				AddPizza(pizza)
			} else {
				result.Items = pizza.Items
				result.Price = pizza.Price

				ModifyPizza(result.ID.Hex(), result)
			}
		}
	}
}

var collection *mongo.Collection

func init() {
	client, err := mongo.NewClient(options.Client().ApplyURI(URI))
	if err != nil {
		log.Fatal(err)
	}

	err = client.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	db := client.Database(DB)
	collection = db.Collection(COLL)

	initPizzas()
}

// AddManyPizzas : Add Many Pizzas
func AddManyPizzas(pizzas []models.IPizza) (payload *models.CreateManyPayload) {
	var allPizzas []interface{}
	for _, pizza := range pizzas {
		allPizzas = append(allPizzas, pizza)
	}

	res, err := collection.InsertMany(context.Background(), allPizzas)
	if err != nil {
		payload = nil
	} else {
		payload = &models.CreateManyPayload{CreatedCount: len(res.InsertedIDs)}
	}
	return
}

// AddPizza : Add Pizza
func AddPizza(pizza models.IPizza) (payload *models.CreatePayload) {
	res, err := collection.InsertOne(context.Background(), pizza)
	if err != nil {
		payload = nil
	} else {
		objID := res.InsertedID.(primitive.ObjectID)
		payload = &models.CreatePayload{CreatedID: objID.Hex()}
	}
	return
}

// ReadAllPizzas : Read All Pizzas
func ReadAllPizzas() (results []*models.IPizza) {
	cur, err := collection.Find(context.Background(), bson.M{}, nil)
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.Background()) {
		var pizza models.IPizza
		err := cur.Decode(&pizza)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &pizza)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	cur.Close(context.Background())
	return
}

// ReadPizza : Read Pizza
func ReadPizza(id string) (result *models.IPizza) {
	objID, _ := primitive.ObjectIDFromHex(id)

	pizza := bson.M{"_id": objID}

	err := collection.FindOne(context.Background(), pizza).Decode(&result)
	if err != nil {
		return nil
	}
	return
}

// ReadPizzaByLabel : Read Pizza By Label
func ReadPizzaByLabel(label string) (result *models.IPizza) {
	pizza := bson.M{"label": label}

	err := collection.FindOne(context.Background(), pizza).Decode(&result)
	if err != nil {
		return nil
	}
	return
}

// ModifyPizza : Modify Pizza
func ModifyPizza(id string, pizza interface{}) (payload *models.UpdatePayload) {
	objID, _ := primitive.ObjectIDFromHex(id)

	_, err := collection.UpdateOne(
		context.Background(),
		bson.M{"_id": objID},
		bson.M{"$set": pizza})

	if err != nil {
		payload = nil
	} else {
		payload = &models.UpdatePayload{UpdatedID: id}
	}
	return
}

// DeleteAllPizzas : Delete All Pizzas
func DeleteAllPizzas() (payload *models.DeleteManyPayload) {
	res, err := collection.DeleteMany(context.Background(), bson.M{})
	if err != nil {
		payload = nil
	} else {
		payload = &models.DeleteManyPayload{DeletedCount: res.DeletedCount}
	}
	return
}

// DeletePizza : Delete Pizza
func DeletePizza(id string) (payload *models.DeletePayload) {
	objID, _ := primitive.ObjectIDFromHex(id)

	res, err := collection.DeleteOne(
		context.Background(),
		bson.M{"_id": objID},
		nil)

	if err != nil {
		payload = nil
	} else {
		if res.DeletedCount == 0 {
			payload = nil
		} else {
			payload = &models.DeletePayload{DeletedID: id}
		}
	}
	return
}
