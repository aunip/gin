package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// IPizza Model
type IPizza struct {
	ID    primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Label string             `json:"label"`
	Items []string           `json:"items"`
	Price float64            `json:"price"`
}
