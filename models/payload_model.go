package models

// CreatePayload Model
type CreatePayload struct {
	CreatedID string `json:"createdId"`
}

// CreateManyPayload Model
type CreateManyPayload struct {
	CreatedCount int `json:"createdCount"`
}

// UpdatePayload Model
type UpdatePayload struct {
	UpdatedID string `json:"updatedId"`
}

// DeletePayload Model
type DeletePayload struct {
	DeletedID string `json:"deletedId"`
}

// DeleteManyPayload Model
type DeleteManyPayload struct {
	DeletedCount int64 `json:"deletedCount"`
}
